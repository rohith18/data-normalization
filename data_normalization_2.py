from xlsxwriter import Workbook
from fuzzywuzzy import fuzz
from c_name_and_manufacturer_name import c_name_set, manufacturer_name_set

c_name_set_3 = set()
manufacturer_name_set_3 = set()

for i in c_name_set:
    i = (" ".join(i.split())).lower()
    replaced_str = i
    replacable_strings = {"pvt":"private", "pvt.":"private", "ltd":"limited", "(p)":"private", "&":"and", "co.":"company", ",":" ", ".":" ", "-":" "}
    for key, value in replacable_strings.items():
        replaced_str = replaced_str.replace(key, value)
        
    if (("(" and ")") in replaced_str):
        replaced_str_2 = replaced_str.replace(replaced_str[ replaced_str.find("(") : replaced_str.find(")") + 1], "")


    replaced_str = (" ".join(replaced_str.split()))
    c_name_set_3.add(replaced_str)

for i in manufacturer_name_set:
    i = (" ".join(i.split())).lower()
    replaced_str = i
    replacable_strings = {"pvt":"private", "pvt.":"private", "ltd":"limited", "(p)":"private", "&":"and", "co.":"company", ",":" ", ".":" ", "-":" "}
    for key, value in replacable_strings.items():
        replaced_str = replaced_str.replace(key, value)
        
    if (("(" and ")") in replaced_str):
        replaced_str_2 = replaced_str.replace(replaced_str[ replaced_str.find("(") : replaced_str.find(")") + 1], "")


    replaced_str = (" ".join(replaced_str.split()))
    manufacturer_name_set_3.add(replaced_str)

print("len(c_name_set): ", len(c_name_set))
print("len(manufacturer_name_set): ", len(manufacturer_name_set))
print("len(c_name_set_3): ", len(c_name_set_3))
print("len(manufacturer_name_set_3): ", len(manufacturer_name_set_3))


workbook = Workbook('similar_data_4.xlsx')
w_sheet = workbook.add_worksheet()

w_sheet.write(0, 0, 'c_name')
w_sheet.write(0, 1, 'manufacturer_name (95%)')

similar_c_name_list = []
similar_manufacturer_name_list = []

for company in c_name_set_3:
    for manufacturer in manufacturer_name_set_3:
        if(fuzz.token_sort_ratio(company,manufacturer) > 95):
            similar_c_name_list.append(company)
            similar_manufacturer_name_list.append(manufacturer)
            
not_similar_c_name_list = [i for i in c_name_set if i not in similar_c_name_list]
not_similar_manufacturer_name_list = [i for i in manufacturer_name_set if i not in similar_manufacturer_name_list]

for i in not_similar_c_name_list:
    if i not in not_similar_manufacturer_name_list:
        similar_c_name_list.append(i)
        similar_manufacturer_name_list.append("")
for i in not_similar_manufacturer_name_list:
    if i not in not_similar_c_name_list:
        similar_c_name_list.append("")
        similar_manufacturer_name_list.append(i)

# Write the column data.
w_sheet.write_column(1, 0, similar_c_name_list)
w_sheet.write_column(1, 1, similar_manufacturer_name_list)

workbook.close()